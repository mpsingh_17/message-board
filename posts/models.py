from django.db import models

class Post(models.Model):
    text = models.TextField()

    def __str__(self):
        ''' A string representation of Post object '''
        return self.text[:30]
